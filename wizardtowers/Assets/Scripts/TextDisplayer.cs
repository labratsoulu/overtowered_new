﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextDisplayer : Photon.MonoBehaviour {

    [SerializeField] Text gameText;

    [PunRPC]
    public void prepareDisplay(string text, int size, float time)
    {
        StartCoroutine(DisplayText(text, size, time));
    }
    
    IEnumerator DisplayText(string text, int size, float time)
    {
        gameText.GetComponent<Text>().text = text;
        gameText.GetComponent<Text>().fontSize = size;
        gameText.GetComponent<Text>().enabled = true;
        if (time != 0)
        {
            yield return new WaitForSeconds(time);
            gameText.GetComponent<Text>().enabled = false;
        }      
    }
}
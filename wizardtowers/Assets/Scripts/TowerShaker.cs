﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShaker : MonoBehaviour {

    public float shakeLength;
    public ParticleSystem shakeEffect;
    public Transform[] thingsToShake;

    private List<Vector3> originalPositions;
    private bool isShaking = false;
    private float shakeTimer = 0;
    private float shakeAmount;

	void Start()
    {
        originalPositions = new List<Vector3>();

        foreach (Transform t in thingsToShake) 
        {
            originalPositions.Add(t.position);
        }
    }
	
	void Update()
    {
		if (isShaking)
        {
            UpdateShaking();
        }
	}

    private void UpdateShaking()
    {
        if (shakeTimer > 0)
        {
            shakeTimer -= Time.deltaTime;

            foreach (Transform t in thingsToShake)
            {
                t.position += Random.insideUnitSphere * shakeAmount;
            }
        }

        else
        {
            shakeTimer = 0;
            isShaking = false;
          
            for (int i = 0; i < thingsToShake.Length; i++)
            {
                thingsToShake[i].position = originalPositions[i];
            }
        }
    }

    public void Shake(float amount)
    {
        shakeEffect.Play();
        isShaking = true;
        shakeAmount = amount / 25f;
        shakeTimer = shakeLength;
    }

    public IEnumerator ExplodeBase()
    {
        foreach (Transform t in thingsToShake)
        {
            t.gameObject.AddComponent<Rigidbody>();
            t.GetComponent<Rigidbody>().AddExplosionForce(25f, transform.position, 15f, 2f, ForceMode.Impulse);
            t.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)), ForceMode.Impulse);
        }

        shakeEffect.Play();
        yield return new WaitForSeconds(1.5f);
        shakeEffect.Play();
        yield return new WaitForSeconds(4f);

        foreach (Transform go in thingsToShake)
        {
            go.gameObject.SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

	[FMODUnity.EventRef]
	public string menuMusic;

	[FMODUnity.EventRef]
	public string endGameMusic;

	[FMODUnity.EventRef]
	public string[] levelMusic;

	public static int bgmTrack = 0;

	// Make this thing a singleton

	public static MusicManager instance;

	void Awake() {
		if(instance == null) {
			instance = this;
		}
		else if(instance != this) {
			Destroy(gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	// The object containing this script should be instantiated in the menu scene
	// (Assuming here that it's the first place where we need background music)

	void Start () {
		instance.MenuMusic ();
	}	

		// The idea is to only have one track playing at a time; Before another track is started, stop the previous one.

	private FMOD.Studio.EventInstance nowPlaying;


	// Music hooks

	public void MenuMusic() {
		if (FmodEventChecker.EventExists (menuMusic)) {
			StopMusic (true);
			nowPlaying = FMODUnity.RuntimeManager.CreateInstance (menuMusic);
			nowPlaying.start ();
		}
	}

	public void VictoryMusic() {
		if (FmodEventChecker.EventExists (endGameMusic)) {
			StopMusic (true);
			nowPlaying = FMODUnity.RuntimeManager.CreateInstance (endGameMusic);
			nowPlaying.setParameterValue ("outcome", 1f);
			nowPlaying.start ();
		}
	}

	public void DefeatMusic() {
		if (FmodEventChecker.EventExists (endGameMusic)) {
			StopMusic (true);
			nowPlaying = FMODUnity.RuntimeManager.CreateInstance (endGameMusic);
			nowPlaying.setParameterValue ("outcome", 0f);
			nowPlaying.start ();
		}
	}

	public void IngameMusic(int level) {
		if (FmodEventChecker.EventExists (levelMusic [level])) {
			StopMusic (true);
			nowPlaying = FMODUnity.RuntimeManager.CreateInstance (levelMusic [level]);
			nowPlaying.start ();
		}
	}
				
	// Handy helper function

	public void StopMusic(bool fadeout) {
		if(nowPlaying != null) {
			if(fadeout) {
				nowPlaying.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
			} else {
				nowPlaying.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
			}
			nowPlaying.release();
		}
	}
}

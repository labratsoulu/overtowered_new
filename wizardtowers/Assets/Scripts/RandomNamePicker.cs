﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomNamePicker : MonoBehaviour {

	public string[] names;
    public string[] wizardNames;
    public string[] prefixes;
    public string[] suffixes;
	public InputField text;

	void Awake()
    {
		//text.text = names [Random.Range (0, names.Length - 1)];

        if (Random.value <= 0.5f)
        {
            text.text = prefixes[Random.Range(0, prefixes.Length - 1)] + " " + wizardNames[Random.Range(0, wizardNames.Length - 1)];
        }

        else
        {
            text.text = wizardNames[Random.Range(0, wizardNames.Length - 1)] + " " + suffixes[Random.Range(0, suffixes.Length - 1)];
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hotkeys : MonoBehaviour {
	public TowerPieceUI[] towerPieces;


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Z)) {
			foreach (TowerPieceUI piece in towerPieces) {
				if (piece.builtType == TowerType.BlockType.DamageSpell && piece.transform.GetChild (0).GetComponent<Slider> ().value == 100) {
					GameObject.FindGameObjectWithTag ("GameManager").GetComponent<SpellList> ().damageSpell (piece);
				} else if(piece.builtType == TowerType.BlockType.BoostSpell && piece.transform.GetChild (0).GetComponent<Slider> ().value == 100){
					GameObject.FindGameObjectWithTag ("GameManager").GetComponent<SpellList> ().boostSpell (piece);
				}
			}
		} else if (Input.GetKeyDown (KeyCode.X)) {
			foreach (TowerPieceUI piece in towerPieces) {
				if (piece.builtType == TowerType.BlockType.HealSpell && piece.transform.GetChild (0).GetComponent<Slider> ().value == 100) {
					GameObject.FindGameObjectWithTag ("GameManager").GetComponent<SpellList> ().healSpell (piece);
				} 
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class TowerSounds : MonoBehaviour {

	[FMODUnity.EventRef]
	public string takBuildEvent;

	[FMODUnity.EventRef]
	public string tipsyBuildEvent;

	[FMODUnity.EventRef]
	public string destructionEvent;


	public void Destruction() {
		if (FmodEventChecker.EventExists (destructionEvent)) {
			FMODUnity.RuntimeManager.PlayOneShot (destructionEvent, transform.position);
		}
	}

	public void BuildPieceTak() {
		if (FmodEventChecker.EventExists (takBuildEvent)) {
			FMODUnity.RuntimeManager.PlayOneShot (takBuildEvent, transform.position);
		}
	}

	public void BuildPieceTipsy() {
		if (FmodEventChecker.EventExists (tipsyBuildEvent)) {
			FMODUnity.RuntimeManager.PlayOneShot (tipsyBuildEvent, transform.position);
		}
	}
}

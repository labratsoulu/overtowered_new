﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionWind : MonoBehaviour {

    public float maxWindMain;
    public float maxWindTurbulence;
    public float windUpDuration;
    public float maxWindTime;

    private WindZone windZone;
    private float windDownDuration;
    private float upInterpolator = 0;
    private float downInterpolator = 0;

	void Start()
    {
        windZone = GetComponent<WindZone>();
        windDownDuration = GetComponent<ParticleSystem>().main.duration;
	}	

    void Update()
    {
        if (upInterpolator < 1)
        {
            upInterpolator += Time.deltaTime / windUpDuration;
            windZone.windMain = Mathf.Lerp(0f, maxWindMain, upInterpolator);
            windZone.windTurbulence = Mathf.Lerp(0f, maxWindTurbulence, upInterpolator);
        }

        else
        {
            StartCoroutine("MaxWindDelay");
            downInterpolator += Time.deltaTime / windDownDuration;
            windZone.windMain = Mathf.Lerp(maxWindMain, 0f, downInterpolator);
            windZone.windTurbulence = Mathf.Lerp(maxWindTurbulence, 0f, downInterpolator);
        }
    }

    private IEnumerator MaxWindDelay()
    {
        yield return new WaitForSeconds(maxWindTime);
    }
}

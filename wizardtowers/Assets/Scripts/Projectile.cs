﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public UnitAI unitAI;
    public GameObject startEffect;
    public ParticleSystem projectileEffect;
    public ParticleSystem hitEffect;
    public GameObject projectileObject;
    public GameObject target;
    public enum projectileTypes { Skeleton, Cultist, Lamiel };
    public projectileTypes projectileType;
    public float damage;
    public int locationNumber = 0;
    public float projectileSpeed;
    public float arcHeight;

    private bool isMoving = true;
    private Vector3 startPos;

    void Start()
    {
        if (startEffect != null)
        {
            GameObject effect = (GameObject)Instantiate(startEffect, transform.position, transform.rotation);
            effect.SetActive(true);
        }

        projectileEffect.Play();
        hitEffect.Stop();
        startPos = transform.position;

        if (locationNumber == 1)
        {
            arcHeight = -arcHeight;
        }

        Destroy(gameObject, 3f);
    }

	void Update ()
    {      
        if (target == null)
        {
            Destroy(gameObject);
        }

        else
        {
            if (isMoving)
            {
                float step = projectileSpeed * Time.deltaTime;

                if (projectileType == projectileTypes.Skeleton)
                {
                    Vector2 xz0 = new Vector2(startPos.x, startPos.z);
                    Vector2 xz1 = new Vector2(target.transform.position.x, target.transform.position.z);
                    float dist = Vector2.Distance(xz1, xz0);
                    Vector2 nextXZ = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.z), xz1, step);
                    float baseY = Mathf.Lerp(startPos.y, target.transform.position.y + target.transform.localScale.y / 2, Vector2.Distance(nextXZ, xz0) / dist);
                    float arc = arcHeight * Vector2.Distance(nextXZ, xz0) * Vector2.Distance(nextXZ, xz1) / (0.25f * dist * dist);
                    Vector3 nextPos = new Vector3(nextXZ.x, baseY + arc, nextXZ.y);
                    transform.position = nextPos;
                }

                else if (projectileType == projectileTypes.Cultist)
                {
                    transform.position = Vector3.MoveTowards(transform.position, target.transform.position + new Vector3(0, target.transform.localScale.y / 2, 0), step);
                }

                else // projectileType == projectileTypes.Lamiel
                {
                    Vector2 xy0 = new Vector2(startPos.x, startPos.y);
                    Vector2 xy1 = new Vector2(target.transform.position.x, target.transform.position.y + target.transform.localScale.y / 2);
                    float dist = Vector2.Distance(xy1, xy0);
                    Vector2 nextXY = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), xy1, step);
                    float baseZ = Mathf.Lerp(startPos.z, target.transform.position.z, Vector2.Distance(nextXY, xy0) / dist);
                    float arc = arcHeight * Vector2.Distance(nextXY, xy0) * Vector2.Distance(nextXY, xy1) / (0.25f * dist * dist);
                    Vector3 nextPos = new Vector3(nextXY.x, nextXY.y, baseZ + arc);
                    transform.position = nextPos;
                }
            }
        }  
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject == target)
        {
            if (projectileObject != null)
            {
                projectileObject.SetActive(false);
            }

            isMoving = false;

            if (GetComponent<Light>() != null)
            {
                GetComponent<Light>().enabled = false;
            }

            projectileEffect.Stop();
            projectileEffect.Clear();
            unitAI.DoDamage();
            hitEffect.Play();
            Destroy(gameObject, hitEffect.main.duration);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Used mainly to check if given event exists in the loaded bank.
public static class FmodEventChecker{

	public static bool checkingEnabled = true; //Set this to false in the final build, when nothing breaks anymore.

	public static FMOD.Studio.EventDescription latestChecked; 

	//If event checking is turned on, try to find the event in the loaded bank(s)
	//If not, always return true.
	public static bool EventExists( string eventPath ) {
		if (!checkingEnabled) {
			return true;
		} else {
			latestChecked = null;
			FMOD.RESULT eventFindResult = FMODUnity.RuntimeManager.StudioSystem.getEvent (eventPath, out latestChecked);
			if (eventFindResult == FMOD.RESULT.OK) {
				return true;
			} else if (eventFindResult == FMOD.RESULT.ERR_EVENT_NOTFOUND) {
				Debug.LogException (new UnityException (string.Format ("\"{0}\" is not a valid FMOD event. Is a reference broken somewhere?", eventPath)));
				return false;
			} else {
				Debug.LogException (new UnityException (string.Format ("Search for FMOD event \"{0}\" failed with result: {1}", eventPath, eventFindResult.ToString ())));
				return false;
			}
		} 
	}
}

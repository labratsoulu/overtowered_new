﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WizardSwitch : MonoBehaviour {

    public Image wizardBackground;
    public Sprite[] wizardPictures;
    public Sprite[] wizardBackgrounds;
    public GameObject activeDescription;
    public GameObject[] wizardDescriptions;
    public int selectedIndex = 0;

    void OnEnable()
    {
        selectedIndex = (int)PhotonNetwork.player.CustomProperties["selectedWizard"];
        wizardBackground.sprite = wizardBackgrounds[selectedIndex];
    }
	
	public void PreviousWizard()
    {
        selectedIndex--;

        if (selectedIndex == -1)
        {
            selectedIndex = wizardBackgrounds.Length - 1;
        }

        wizardBackground.sprite = wizardBackgrounds[selectedIndex];
        activeDescription.SetActive(false);
        wizardDescriptions[selectedIndex].SetActive(true);
        activeDescription = wizardDescriptions[selectedIndex];

    }

    public void NextWizard()
    {
        selectedIndex++;

        if (selectedIndex == wizardBackgrounds.Length)
        {
            selectedIndex = 0;
        }

        wizardBackground.sprite = wizardBackgrounds[selectedIndex];
        activeDescription.SetActive(false);
        wizardDescriptions[selectedIndex].SetActive(true);
        activeDescription = wizardDescriptions[selectedIndex];
    }
}
﻿using UnityEngine;
using System.Collections;

public class PathToggle : MonoBehaviour {

    void Toggle(GameObject block)
    {
        block.SetActive(!block.activeInHierarchy);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFrameColor : MonoBehaviour {

	public Sprite redFrame;
	public Sprite blueFrame;
	public bool activePlayerUI;

	// Use this for initialization
	void Start () {
		if ((activePlayerUI && PhotonNetwork.player.ID == 1) || (!activePlayerUI && PhotonNetwork.player.ID != 1)) {
			GetComponent<Image> ().sprite = redFrame;
		} else {
			GetComponent<Image> ().sprite = blueFrame;
		}
	}

}

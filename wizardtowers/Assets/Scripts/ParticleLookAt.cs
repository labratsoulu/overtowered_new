﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLookAt : MonoBehaviour {

    public Transform systemToTurn;
    public float yOffset;
    private UnitAI unitAI;
    private ParticleSystem mainSystem;
    private bool isTurned = false;

	void Start()
    {
        mainSystem = GetComponent<ParticleSystem>();
        unitAI = transform.root.GetComponent<UnitAI>();
    }
	
	void Update()
    {
        if (mainSystem.isPlaying && unitAI.initialTarget != null && !isTurned)
        {
            isTurned = true;
            systemToTurn.LookAt(unitAI.initialTarget.transform.position + new Vector3(0f, yOffset, 0f));
        }
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using RTS_Cam;

public class NetworkManager : MonoBehaviour {

    [SerializeField] Text connectionText;
    [SerializeField] Camera sceneCamera;
    //[SerializeField] AudioListener sceneListener;

    [SerializeField] GameObject lobbyWindow;
    [SerializeField] InputField playerName;
    [SerializeField] InputField roomName;
    [SerializeField] InputField roomList;

    [SerializeField] GameObject playerListWindow;
    [SerializeField] Text player1Name;
    [SerializeField] Text player2Name;
    [SerializeField] Text player1Health;
    [SerializeField] Text player2Health;
    [SerializeField] Slider player1HealthSlider;
    [SerializeField] Slider player2HealthSlider;
    [SerializeField] Text magicText;
    [SerializeField] Slider magicSlider;

    [SerializeField] InputField messageWindow;

    [SerializeField] GameObject gameUI;
    [SerializeField] RTS_Camera cameraScript;
    [SerializeField] Transform[] cameraPositions;

    public GameObject textDisplayer;
    AudioSource statusAudio;
    public AudioClip startSound;
    Queue<string> messages;
    const int messageCount = 6;
    PhotonView photonView;

    public GameObject blueBase;
    public GameObject redBase;
    public LayerMask ignoreLayer;

    public Constants constants;

    // Use this for initialization
    void Start ()
    {        
        photonView = GetComponent<PhotonView>();
        statusAudio = GetComponent<AudioSource>();
        messages = new Queue<string>(messageCount);

        //PhotonNetwork.autoCleanUpPlayerObjects = false;
        PhotonNetwork.logLevel = PhotonLogLevel.Full;

        if (constants.offlineMode)
        {
            PhotonNetwork.offlineMode = true;
            lobbyWindow.SetActive(true);
        }

        else
        {
            PhotonNetwork.ConnectUsingSettings(constants.versionNumber);
            StartCoroutine("UpdateConnectionString");
            PhotonNetwork.autoJoinLobby = true;
        }              

        sceneCamera.transform.position = cameraPositions[0].position;
        sceneCamera.transform.rotation = cameraPositions[0].rotation;

        player1HealthSlider.maxValue = constants.towerStartHealth;
        player2HealthSlider.maxValue = constants.towerStartHealth;
        player1HealthSlider.value = constants.towerStartHealth;
        player2HealthSlider.value = constants.towerStartHealth;
        player1Health.text = constants.towerStartHealth.ToString();
        player2Health.text = constants.towerStartHealth.ToString();
        magicSlider.maxValue = constants.maxMagic;
    }

	private IEnumerator UpdateConnectionString()
    {
        while (true)
        {
            connectionText.text = PhotonNetwork.connectionStateDetailed.ToString();
            yield return null;
        }
	}

    void OnJoinedLobby()
    {
        lobbyWindow.SetActive(true);
    }

    void OnReceivedRoomListUpdate()
    {
        roomList.text = "";
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();
        foreach (RoomInfo room in rooms)
        {
            roomList.text += room.name + "\n";
        }
    }

    public void JoinRoom()
    {
        PhotonNetwork.player.name = playerName.text;
        RoomOptions ro = new RoomOptions() { isVisible = true, maxPlayers = constants.maxPlayers };
        PhotonNetwork.JoinOrCreateRoom(roomName.text, ro, TypedLobby.Default);
    }

    private void OnJoinedRoom()
    {
        lobbyWindow.SetActive(false);
        StopCoroutine("UpdateConnectionString");
        connectionText.text = "";
        playerListWindow.SetActive(true);
        cameraScript.enabled = true;

        // Joined players have different colors in message window. Also they are given ownership of their base and told which layer to ignore when building.
        if (PhotonNetwork.playerList.Length == 1)
        {
            AddMessage("<color=red>" + PhotonNetwork.player.name + "</color> <color=green>joined the game</color>");      
            GameObject.FindWithTag("Player1").GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
            blueBase.layer = ignoreLayer;
            sceneCamera.transform.position = cameraPositions[1].position;
            sceneCamera.transform.rotation = cameraPositions[1].rotation;
        }

        else if (PhotonNetwork.playerList.Length == 2)
        {
            AddMessage("<color=blue>" + PhotonNetwork.player.name + "</color> <color=green>joined the game</color>");
            GameObject.FindWithTag("Player2").GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
            redBase.layer = ignoreLayer;
            sceneCamera.transform.position = cameraPositions[2].position;
            sceneCamera.transform.rotation = cameraPositions[2].rotation;
        }

        sceneCamera.enabled = true;
        //sceneListener.enabled = true;
        photonView.RPC("UpdatePlayerList_RPC", PhotonTargets.AllBuffered, PhotonNetwork.player.ID, PhotonNetwork.player.name);
        photonView.RPC("UpdateHealthList_RPC", PhotonTargets.AllBuffered, PhotonNetwork.player.ID, constants.towerStartHealth);
        UpdateMagicValue(constants.magicStartAmount);

        //textDisplayer.GetComponent<TextDisplayer>().prepareDisplay("<color=#F31E1E>Waiting for another player...</color>", 40, 0f);
        textDisplayer.GetComponent<PhotonView>().RPC("prepareDisplay", PhotonTargets.All, "<color=#F31E1E>Waiting for another player...</color>", 40, 0f);

        if (PhotonNetwork.playerList.Length == constants.playersToStart)
        {
            //statusAudio.PlayOneShot(startSound, 0.4f);
            photonView.RPC("StartGame_RPC", PhotonTargets.All);
        }
    }

    //Display message and clear player fields on d/c
    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        //photonView.RPC("UpdatePlayerList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, PhotonNetwork.player.name);
        AddMessage_RPC("<color=red>" + player.name + " left the game</color>");
        photonView.RPC("UpdatePlayerList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, "");
        photonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, null);
    }

    [PunRPC]
    void StartGame_RPC()
    {
        print("Game Start!");
        gameUI.SetActive(true);
        textDisplayer.GetComponent<PhotonView>().RPC("prepareDisplay", PhotonTargets.All, "<color=#63FF77FF>Fight!</color>", 80, 3f);
    }

    void AddMessage(string message)
    {
        photonView.RPC("AddMessage_RPC", PhotonTargets.All, message);
    }

    [PunRPC]
    void AddMessage_RPC(string message)
    {
        messages.Enqueue(message);

        if (messages.Count > messageCount)
        {
            messages.Dequeue();
        }

        messageWindow.text = "";

        foreach (string m in messages)
        {
            messageWindow.text += m + "\n";
        }      
    }

    [PunRPC]
    void UpdatePlayerList_RPC(int playerID, string name)
    {
        if (playerID == 1)
        {
            player1Name.text = name;
        }
        else //playerID == 2
        {
            player2Name.text = name;
        }
    }

    [PunRPC]
    void UpdateHealthList_RPC(int playerID, float health)
    {
        if (playerID == 1)
        {
            player1Health.text = health.ToString();
            player1HealthSlider.value = health;
        }
        else //playerID == 2
        {
            player2Health.text = health.ToString();
            player2HealthSlider.value = health;
        }
    }

    public void UpdateMagicValue(float magic)
    {
		magicText.text = magic.ToString();
        magicSlider.value = magic;
    }

    [PunRPC]
    public void EndGame(int playerID)
    {
        string msg;

        if (playerID == PhotonNetwork.player.ID)
        {
            msg = "<color=#F31E1E>You lose!\n <size=25>Press F1 to play again or Esc to quit.</size></color>";
        }

        else
        {
            msg = "<color=#63FF77FF>You win!\n <size=25>Press F1 to play again or Esc to quit.</size></color>";
        }
        gameUI.SetActive(false);
        textDisplayer.GetComponent<TextDisplayer>().prepareDisplay(msg, 40, 0f);
    }
}

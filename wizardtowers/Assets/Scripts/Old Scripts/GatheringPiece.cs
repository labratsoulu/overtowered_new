﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GatheringPiece : MonoBehaviour {

	private Constants constants;

	private float spawnRate;
	private int gatheringNumber = 10;

	private List<float> golemSpawns; 
	private List<float> apprenticeSpawns; 
	private List<float> skeletonSpawns; 

	private List<string> units;

	private TowerBaseSpawner spawner;

	// Use this for initialization
	void Start () {
		constants = GameObject.FindGameObjectWithTag ("Constants").GetComponent<Constants>();
		spawnRate = constants.unitSpawnRate;

		golemSpawns = new List<float> ();
		apprenticeSpawns = new List<float> ();
		skeletonSpawns = new List<float> ();

		units = new List<string> ();
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < golemSpawns.Count; i++){
			golemSpawns[i] += Time.deltaTime;
			if (golemSpawns[i] >= spawnRate) {
				golemSpawns[i] = 0f;
				units.Add ("golem");
			}
		}
		for (int i = 0; i < apprenticeSpawns.Count; i++){
			apprenticeSpawns[i] += Time.deltaTime;
			if (apprenticeSpawns[i] >= spawnRate) {
				apprenticeSpawns[i] = 0f;
				units.Add ("apprentice");
			}
		}
		for (int i = 0; i < skeletonSpawns.Count; i++){
			skeletonSpawns[i] += Time.deltaTime;
			if (skeletonSpawns[i] >= spawnRate) {
				skeletonSpawns[i] = 0f;
				units.Add ("skeleton");
			}
		}

		if (units.Count >= gatheringNumber) {
			if (spawner == null) {//the first time a group is ready, look for the towerbase
				spawner = transform.root.gameObject.GetComponent<TowerBaseSpawner> ();
			}

			foreach (string unit in units)
				spawner.spawnUnit (unit);
			units = new List<string> ();
		}
	}

	public void addTowerPiece(TowerType.BlockType type){
		switch (type) {
		case TowerType.BlockType.ApprenticeSpawn:
			apprenticeSpawns.Add (0f);
			break;
		case TowerType.BlockType.GolemSpawn:
			golemSpawns.Add (0f);
			break;
		case TowerType.BlockType.SkeletonSpawn:
			skeletonSpawns.Add (0f);
			break;
		}
	}
}

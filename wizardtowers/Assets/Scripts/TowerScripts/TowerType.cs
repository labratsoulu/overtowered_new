﻿using UnityEngine;
using System.Collections;

public class TowerType : MonoBehaviour {

	public BlockType Type;

	public enum BlockType {
		Empty, 
		GolemSpawn, 
		SkeletonSpawn, 
		ApprenticeSpawn,
        EmmettSpawn,
        SarkoSpawn,
        LamielSpawn,
        DamageUpgrade, 
		HealthUpgrade, 
		RangeUpgrade, 
		SpeedUpgrade, 
		CooldownUpgrade,
		VolatileUpgrade,
		GatheringPiece,
		DamageSpell,
		BoostSpell,
		HealSpell,
		MagicRegen,
		MaxMagic,
		ExplodingUnits
	};
}

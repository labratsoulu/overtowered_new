﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadiusTester : MonoBehaviour {

    public float radius;

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.6f, 1f, 0f, 0.5f);
        Gizmos.DrawSphere(transform.position, radius);
    }
}

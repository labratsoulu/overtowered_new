﻿using UnityEngine;
using System.Collections;

public class UnitSpawner : MonoBehaviour {

	public GameObject redBase;
	public GameObject blueBase;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SpawnUnit(string unit)
    {
		if (PhotonNetwork.player.ID == 1)
		{
			redBase.GetComponent<TowerBaseSpawner> ().spawnUnit (unit);
		}

		else // ID == 2
		{
			blueBase.GetComponent<TowerBaseSpawner> ().spawnUnit (unit);
		}
    }
}

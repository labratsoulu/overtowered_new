﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMixManager : MonoBehaviour {

	static float[] volumeLevels;
	public static AudioMixManager ammInstance;

	//Mixer channel level controls
	string[] vcaAddrs = new string[] {
		"vca:/MasterVolume",
		"vca:/Music",
		"vca:/Game",
		"vca:/Voiceover",
		"vca:/Ambient",
		"vca:/UI"
	};

	FMOD.Studio.VCA[] vcas = new FMOD.Studio.VCA[6]; 

	//Preferences
	float[] defaultVolumeLevels = new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f}; //Fallback values in case not set in playerprefs
	string[] prefLabels = new string[]{"volume_master", "volume_music", "volume_game", "volume_voice", "volume_ambience", "volume_ui"};

	void Awake() {
		if (ammInstance == null) {
			ammInstance = this;
		} else if (ammInstance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	void Start () {
		for (int i = 0; i < vcas.Length; i++) {
			vcas [i] = FMODUnity.RuntimeManager.GetVCA (vcaAddrs [i]);
		}
		
		if (volumeLevels == null) {
			volumeLevels = new float[6];
			SetLevelsToPreferences ();
		} 
		SetLevels (volumeLevels);

	}

	public void SetChannelVolume(int c, float vol) {
		vcas [c].setVolume (vol);
		volumeLevels [c] = vol;
		PlayerPrefs.SetFloat (prefLabels [c], vol);
	}

	public void SetLevels(float[] vols) {
		for (int i = 0; i < vcas.Length; i++) {
			SetChannelVolume (i, vols [i]);
		}
	}

	public void SetLevelsToPreferences() {
		float[] levels = new float[6];

		for (int i = 0; i < prefLabels.Length; i++) {
			if (PlayerPrefs.HasKey (prefLabels [i])) {
				levels [i] = PlayerPrefs.GetFloat (prefLabels [i]);
			} else {
				levels [i] = defaultVolumeLevels [i];
				PlayerPrefs.SetFloat (prefLabels [i], defaultVolumeLevels [i]);
			}
		}
		PlayerPrefs.Save ();

		volumeLevels = levels;
	}

	public float[] GetLevels() {
		return volumeLevels;
	}
}

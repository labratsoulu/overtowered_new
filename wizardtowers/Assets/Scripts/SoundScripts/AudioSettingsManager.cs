﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSettingsManager : MonoBehaviour {

	public enum AudioChannel { master, music, game, voice, ambience, ui };

	//[FMODUnity.EventRef] public string valueChangeEvent;
	//[FMODUnity.EventRef] public string dragStartSound;
	//[FMODUnity.EventRef] public string dragEndSound;

	//public GameObject mixManagerObject;
	AudioMixManager mixManager;

	bool silentValueChange;
	bool initializing;

	[FMODUnity.EventRef]
	public string valueChangeSoundPath ="event:/ui/highlight";

	FMOD.Studio.EventInstance valueChangeSound;

	//UI hooks
	public Text[] levelTexts;
	public Slider[] levelSliders;

	// Use this for initialization
	void Start () {
		mixManager = GameObject.FindGameObjectWithTag ("audioMixManager").GetComponent<AudioMixManager> ();

		if (FmodEventChecker.EventExists (valueChangeSoundPath)) {
			valueChangeSound = FMODUnity.RuntimeManager.CreateInstance ("event:/ui/highlight");
		}

		InitSliders ();
	}

	void OnEnable() {
		MuteSliderSounds (false);
	}

	void OnDisable() {
		MuteSliderSounds (true);
		PlayerPrefs.Save ();
	}

	void MuteSliderSounds( bool _silentValueChange ) {
		silentValueChange = _silentValueChange;
	}

	public void SetVolumeFromSlider( int c ) {
		if (!silentValueChange) {
			//valueChangeSound.start ();
		}

		float volume = levelSliders [c].value;

		if (volume > 1.0f) {
			volume = 1.0f;
		}
		if (volume < 0.0f) {
			volume = 0.0f;
		}

		if (!initializing) {
			mixManager.SetChannelVolume (c, volume);
		}
		SetLevelText (c, volume);
	}

	public void SetLevelText( int c, float volume ) {
		string levelTxt = (volume).ToString ("0%");
		levelTexts [c].text = levelTxt;
	}

	//Sets the sliders of all channels
	public void SetLevels( float[] channelLevels ) {
		for (int i = 0; i < levelSliders.Length; i++) {
			levelSliders [i].value = channelLevels [i];
			SetLevelText (i, channelLevels [i]);
		}
	}

	void InitSliders() {
		float[] levels = mixManager.GetLevels();
		MuteSliderSounds (true);
		initializing = true;
		for (int i = 0; i < levelSliders.Length; i++) {
			levelSliders [i].value = levels [i];
			SetLevelText (i, levels [i]);
		}
		initializing = false;
		MuteSliderSounds (false);
	}
}

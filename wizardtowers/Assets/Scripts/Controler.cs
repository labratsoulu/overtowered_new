﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Controler : MonoBehaviour {

	private RectTransform rt;

	private bool wasSetNeutralH;
	private bool wasSetNeutralV;

	private float moveTimer = 1;
	private float timeMoved;
	private bool shrinking;

	public GameObject focus;

	// Use this for initialization
	void Start () {
		rt = GetComponent<RectTransform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis ("Horizontal") == 0) {
			wasSetNeutralH = true;
		}
		if (Input.GetAxis ("Horizontal") > 0 && wasSetNeutralH) {
			wasSetNeutralH = false;
			TowerPieceUI element = focus.GetComponent<TowerPieceUI> ();
			if (element != null) {
				if (element.right != null) {
					setFocus (element.right.gameObject);
				}
			} else {
				if(focus.GetComponent<CursorGuide> ().right != null)
					setFocus(focus.GetComponent<CursorGuide> ().right);
			}
		} else if (Input.GetAxis ("Horizontal") < 0 && wasSetNeutralH) {
			wasSetNeutralH = false;
			TowerPieceUI element = focus.GetComponent<TowerPieceUI> ();
			if (element != null) {
				if (element.left != null) {
					setFocus (element.left.gameObject);
				}
			} else {
				if(focus.GetComponent<CursorGuide> ().left != null)
					setFocus(focus.GetComponent<CursorGuide> ().left);
			}
		}

		if (Input.GetAxis ("Vertical") == 0) {
			wasSetNeutralV = true;
		}
		if (Input.GetAxis ("Vertical") > 0 && wasSetNeutralV) {
			wasSetNeutralV = false;
			TowerPieceUI element = focus.GetComponent<TowerPieceUI> ();
			if (element != null) {
				if (element.up != null) {
					setFocus (element.up.gameObject);
				}
			} else {
				if(focus.GetComponent<CursorGuide> ().up != null)
					setFocus(focus.GetComponent<CursorGuide> ().up);
			}
		} else if (Input.GetAxis ("Vertical") < 0 && wasSetNeutralV) {
			wasSetNeutralV = false;
			TowerPieceUI element = focus.GetComponent<TowerPieceUI> ();
			if (element != null) {
				if (element.down != null) {
					setFocus (element.down.gameObject);
				}
			} else {
				if(focus.GetComponent<CursorGuide> ().down != null)
					setFocus(focus.GetComponent<CursorGuide> ().down);
			}
		}

		if (Input.GetButtonDown ("Submit")) {
			GameObject newTarget = null;
			if (focus.GetComponent<TowerPieceUI> () == null) {
				//setFocus(focus.transform.parent.GetComponent<RMenu> ().UIPiece);
				Debug.Log(focus.transform.parent.GetComponent<RMenu> ().UIPiece);
				newTarget = focus.transform.parent.GetComponent<RMenu> ().UIPiece;
			}
			var pointer = new PointerEventData (EventSystem.current);
			ExecuteEvents.Execute (focus, pointer, ExecuteEvents.submitHandler);
			if(newTarget != null)
				setFocus (newTarget);
			else if (GameObject.FindGameObjectWithTag ("RMenu") != null) {
				setFocus (GameObject.FindGameObjectWithTag ("RMenu").transform.GetChild (0).gameObject);
			} 
		}

		if (shrinking) {
			timeMoved -= Time.deltaTime;
			rt.sizeDelta = new Vector2 (rt.sizeDelta.x - Time.deltaTime * 20, rt.sizeDelta.y - Time.deltaTime * 20);
			if (timeMoved <= 0) {
				shrinking = false;
			}
		} else {
			timeMoved += Time.deltaTime;
			rt.sizeDelta = new Vector2 (rt.sizeDelta.x + Time.deltaTime * 20, rt.sizeDelta.y + Time.deltaTime * 20);
			if (timeMoved >= moveTimer) {
				shrinking = true;
			}
		}
		transform.position = focus.transform.position;
	}

	private void setFocus(GameObject newTarget){
		focus = newTarget;
		//GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		transform.position = newTarget.transform.position;
	}
}

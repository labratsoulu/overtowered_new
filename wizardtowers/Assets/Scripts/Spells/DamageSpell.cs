﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DamageSpell : MonoBehaviour {

	protected RaycastHit hit;

	public TowerPieceUI piece;

	[FMODUnity.EventRef]
	public string sfxEvent = "event:/spells/damage";

    private Camera cam;
    private GameObject playerCamera;
	private float spellCost;
	private LayerMask enemyLayer;
	private float damage;
	private Constants constants;
    private UnitExploder exploder;
	private PlayerVars playerVars;
    private PhotonView animView;
    private string spellsound = "event:/Spells/AoE";

	void Awake()
	{
		//constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
		if (PhotonNetwork.player.ID == 1)
		{
			//blue player
			enemyLayer = LayerMask.NameToLayer("Player2");
			playerVars = GameObject.FindWithTag("Player1").GetComponent<PlayerVars>();
            playerCamera = GameObject.FindWithTag("Player1Camera");
        }

		else // playerID == 2
		{
			enemyLayer = LayerMask.NameToLayer("Player1");
			playerVars = GameObject.FindWithTag("Player2").GetComponent<PlayerVars>();
            playerCamera = GameObject.FindWithTag("Player2Camera");
        }

        cam = playerCamera.GetComponent<Camera>();
        animView = playerCamera.GetComponent<PhotonView>();
        constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
        exploder = GameObject.FindWithTag("GameManager").GetComponent<UnitExploder>();
    }

    void Start()
    {
        spellCost = Mathf.Round(constants.damageSpellCost * piece.spellCostDown);
        damage = Mathf.Round(constants.damageSpellDamage * piece.spellEffectUp);
    }

    // Update is called once per frame
    void Update()
	{
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
		{
			transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
		}

		EndDrag();
	}

	protected void EndDrag()
	{
		if (Input.GetMouseButtonDown (0) && !EventSystem.current.IsPointerOverGameObject())
        {
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().arcaneBomb ();
			playerVars.calculateMagic(-spellCost);
			piece.resetTimer ();

			if (hit.transform != null)
            {
				GameObject obj = PhotonNetwork.Instantiate("FireExplosionEffect", transform.position + new Vector3(0, 5f, 0), Quaternion.identity, 0);
                animView.RPC("SetAnimatorTrigger", PhotonTargets.All, "DamageCast");
                exploder.GetComponent<PhotonView>().RPC("ExplodeAtPoint", PhotonTargets.All, transform.position, PhotonNetwork.player.ID, damage);

				FMOD.Studio.EventInstance sfxInstance = null;
				if (FmodEventChecker.EventExists (sfxEvent)) {
					sfxInstance = FMODUnity.RuntimeManager.CreateInstance (sfxEvent);
				}
				if(sfxInstance != null) {
					sfxInstance.set3DAttributes (FMODUnity.RuntimeUtils.To3DAttributes (hit.transform));
					sfxInstance.start ();
					sfxInstance.release ();
				}
			}

			Destroy(gameObject);
		}

        else if (Input.GetMouseButtonDown(1))
        {
			Destroy(gameObject);
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class ActivePieces : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void activateAll(){
		foreach (Transform child in transform) {
			TowerPieceUI temp = child.GetComponent<TowerPieceUI> ();
			if (temp != null && temp.builtType != TowerType.BlockType.Empty && !temp.active && (temp.builtType == TowerType.BlockType.EmmettSpawn || temp.builtType == TowerType.BlockType.LamielSpawn || temp.builtType == TowerType.BlockType.SarkoSpawn)) {
				temp.toggle ();
			}
		}
	}

	public void deactivateAll(){
		foreach (Transform child in transform) {
			TowerPieceUI temp = child.GetComponent<TowerPieceUI> ();
			if (temp != null && temp.builtType != TowerType.BlockType.Empty && temp.active && (temp.builtType == TowerType.BlockType.EmmettSpawn || temp.builtType == TowerType.BlockType.LamielSpawn || temp.builtType == TowerType.BlockType.SarkoSpawn)) {
				temp.toggle ();
			}
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

public class RoomButtonClick : MonoBehaviour {

    private Button button;
    public string roomName = "";
    public NetworkLobby networkLobby;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(JoinThisRoom);
    }

    private void JoinThisRoom()
    {
        networkLobby.JoinSelectedRoom(roomName);
    }
}

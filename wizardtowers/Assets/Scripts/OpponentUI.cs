﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OpponentUI : MonoBehaviour {

	public Sprite Empty;
	public Sprite Emmet;
	public Sprite Lamiel;
	public Sprite Sarko;
	public Sprite boostSpell;
	public Sprite damageSpell;
	public Sprite healSpell;
	public Sprite magicRegen;
	public Sprite maxMagic;
	public Sprite explodingUnits;

	private Image image;

	public Sprite redFrame;
	public Sprite blueFrame;

	public Image frameObject;

	void Awake(){
		image = GetComponent<Image> ();
	}

	[PunRPC]
	void informOpponent(string spriteName){
		switch (spriteName) {
		case "Empty":
			image.sprite = Empty;
			image.enabled = false;
			frameObject.enabled = false;
			break;
		case "emmet":
			image.sprite = Emmet;
			setBackground ();
			break;
		case "lamiel":
			image.sprite = Lamiel;
			setBackground ();
			break;
		case "sarko":
			image.sprite = Sarko;
			setBackground ();
			break;
		case "boostSpell":
			image.sprite = boostSpell;
			setBackground ();
			break;
		case "damageSpell":
			image.sprite = damageSpell;
			setBackground ();
			break;
		case "healSpell":
			image.sprite = healSpell;
			setBackground ();
			break;
		case "maxMagic":
			image.sprite = maxMagic;
			setBackground ();
			break;
		case "magicRegen":
			image.sprite = magicRegen;
			setBackground ();
			break;
		case "explodingUnits":
			image.sprite = explodingUnits;
			setBackground ();
			break;
		}
	}

	private void setBackground(){
		image.enabled = true;
		frameObject.enabled = true;
		if (PhotonNetwork.player.ID == 1) {
			frameObject.sprite = blueFrame;
		} else {
			frameObject.sprite = redFrame;
		}
	}
}

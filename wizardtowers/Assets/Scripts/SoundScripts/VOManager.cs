﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
//	DATA CLASSES
//

[System.Serializable]
public class VODictionaryEntry {
	[Tooltip("Pass this as an argument to the play method")]
	public string key;

	[FMODUnity.EventRef]
	public string fmodEvent;
}

[System.Serializable]
public class WizardVODictionaryEntry {
	[Tooltip("Pass this as an argument to the play method")]
	public string key;

	[FMODUnity.EventRef]
	public string turbyFmodEvent;

	[FMODUnity.EventRef]
	public string takFmodEvent;

}

//
//	MAIN CLASS
//

public class VOManager : MonoBehaviour {
	[TextArea]
	public string notes="Be careful when adjusting array sizes.";

	private Dictionary<string, string> voEvents;

	[Tooltip("Add tutorial voiceover entries here")]
	public VODictionaryEntry[] TutorialVos;

	[Tooltip("Add wizard voiceover entries here")]
	public WizardVODictionaryEntry[] WizardVos;

	[Tooltip("Please leave these alone - or at least the array size.")]
	public string[] wizardPrefixes;

	private FMOD.Studio.EventInstance currentTutorialVO;
	private FMOD.Studio.EventInstance currentWizardVO;

	//default to Turby's voice
	int wizard = 0;

	//singletonize
	public static VOManager instance;
	void Awake() {
		if(instance == null) {
			instance = this;
		}
		else if(instance != this) {
			Destroy(gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	//build dictionary
	void Start() {
		voEvents = new Dictionary<string, string> (TutorialVos.Length);
		foreach (VODictionaryEntry vde in TutorialVos) {
			voEvents.Add (vde.key, vde.fmodEvent);
		}
		foreach (WizardVODictionaryEntry vde in WizardVos) {
			voEvents.Add (wizardPrefixes[0] + vde.key, vde.turbyFmodEvent);
			voEvents.Add (wizardPrefixes[1] + vde.key, vde.takFmodEvent);
		}
	}

	//clean up events that are done playing
	void Update() {

		FMOD.Studio.PLAYBACK_STATE eventState;

		if (currentTutorialVO != null) {
			currentTutorialVO.getPlaybackState (out eventState);
			if (eventState == FMOD.Studio.PLAYBACK_STATE.STOPPED) {
				currentTutorialVO.release ();
			}
		}
		if (currentWizardVO != null) {
			currentWizardVO.getPlaybackState (out eventState);
			if (eventState == FMOD.Studio.PLAYBACK_STATE.STOPPED) {
				currentWizardVO.release ();
			}
		}

	}

	public void PlayTutorialVO(string voName) {
		string voPath;
		voEvents.TryGetValue (voName, out voPath);
		if (FmodEventChecker.EventExists (voPath)) {
			if (currentTutorialVO != null) {
				currentTutorialVO.stop (FMOD.Studio.STOP_MODE.IMMEDIATE);
				currentTutorialVO.release ();
			}
			currentTutorialVO = FMODUnity.RuntimeManager.CreateInstance (voPath);
			if (currentTutorialVO != null) {
				currentTutorialVO.start ();
			}
		}
	}

	public void PlayWizardVO(string voName) {
		string voPath;
		wizard = (int)PhotonNetwork.player.CustomProperties ["selectedWizard"];

		voEvents.TryGetValue (wizardPrefixes[wizard]+voName, out voPath);

		if (FmodEventChecker.EventExists (voPath)) {
			if (currentWizardVO != null) {
				currentWizardVO.stop (FMOD.Studio.STOP_MODE.IMMEDIATE);
				currentWizardVO.release ();
			}
			currentWizardVO = FMODUnity.RuntimeManager.CreateInstance (voPath);
			if (currentWizardVO != null) {
				currentWizardVO.start ();
			}
		}
	}

	public void SetWizard(int _wizard) {
		wizard = _wizard;
	}
}

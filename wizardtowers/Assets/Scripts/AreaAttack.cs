﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaAttack : MonoBehaviour {

    public UnitAI unitAI;
    public float attackRadius;
    public Collider[] unitColliders;
    public ParticleSystem areaAttackEffect;

    // Debug code to show attack area
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1f, 0f, 0f, 0.5f);
        Gizmos.DrawSphere(transform.position, attackRadius);
    }

    // The AoE attack itself
    public void Attack()
    {
        unitColliders = Physics.OverlapSphere(transform.position, attackRadius, unitAI.targetLayer);
        foreach (Collider unit in unitColliders)
        {
            if (PhotonNetwork.isMasterClient)
            {
                unit.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, unitAI.damage);
            }
        }

        if (unitAI.GetComponent<UnitSounds>() != null)
        {
            unitAI.GetComponent<UnitSounds>().HitSound(unitAI.currentTarget);
        }

        areaAttackEffect.Play();
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerBaseSpawner : MonoBehaviour {

	public GameObject golem;
	public GameObject apprentice;
	public GameObject skeleton;
    public GameObject emmett;
    public GameObject sarko;
    public GameObject lamiel;

    public Transform spawnLocation;
    public Constants constants;
    public PlayerVars playerVars;

    private float spawnRadius;
    private float spawnRate;
    private float unitValue;
    private string unitToSpawn;

    void Awake()
    {
        spawnRadius = constants.unitSpawnRadius;
        spawnRate = constants.unitSpawnRate;
        unitValue = 0;
        unitToSpawn = "";
    }


	public void spawnUnit(string unitName){

        if (playerVars.unitAmount < constants.maxUnitAmount)
        {
            switch (unitName)
            {
                case "apprentice":
                    unitToSpawn = apprentice.name;
                    unitValue = constants.apprenticeValue;
                    break;
                case "skeleton":
                    unitToSpawn = skeleton.name;
                    unitValue = constants.skeletonValue;
                    break;
                case "golem":
                    unitToSpawn = golem.name;
                    unitValue = constants.golemValue;
                    break;
                case "emmett":
                    unitToSpawn = emmett.name;
                    unitValue = constants.emmettValue;
                    break;
                case "sarko":
                    unitToSpawn = sarko.name;
                    unitValue = constants.sarkoValue;
                    break;
                case "lamiel":
                    unitToSpawn = lamiel.name;        
                    unitValue = constants.lamielValue;
                    break;
            }

            if (constants.testMode || constants.offlineMode)
            {
                PhotonNetwork.Instantiate(unitToSpawn, spawnLocation.position, spawnLocation.rotation, 0);
            }
            
            else
            {
                if (playerVars.unitAmount + unitValue <= constants.maxUnitAmount)
                {
                    PhotonNetwork.Instantiate(unitToSpawn, spawnLocation.position, spawnLocation.rotation, 0);
                    playerVars.changeUnitAmount((int)unitValue);
                }
            }                
        }
	}

	public void spawnUnitByType(TowerType.BlockType type, float damageMultiplier, float healthMultiplier, bool exploding)
    {

		if (playerVars.unitAmount < constants.maxUnitAmount)
		{
			switch (type)
			{
			    case TowerType.BlockType.ApprenticeSpawn:
                    unitToSpawn = apprentice.name;
                    unitValue = constants.apprenticeValue;
                    break;
			    case TowerType.BlockType.SkeletonSpawn:
                    unitToSpawn = skeleton.name;
                    unitValue = constants.skeletonValue;
                    break;
			    case TowerType.BlockType.GolemSpawn:
                    unitToSpawn = golem.name;
                    unitValue = constants.golemValue;
                    break;
                case TowerType.BlockType.EmmettSpawn:
                    unitToSpawn = emmett.name;
                    unitValue = constants.emmettValue;
                    break;
                case TowerType.BlockType.SarkoSpawn:
                    unitToSpawn = sarko.name;
                    unitValue = constants.sarkoValue;
                    break;
                case TowerType.BlockType.LamielSpawn:
                    unitToSpawn = lamiel.name;
                    unitValue = constants.lamielValue;
                    break;
            }

            if (constants.testMode || constants.offlineMode)
            {
                GameObject unit = PhotonNetwork.Instantiate(unitToSpawn, spawnLocation.position, spawnLocation.rotation, 0);
                unit.GetComponent<PhotonView>().RPC("SyncValues", PhotonTargets.All, damageMultiplier, healthMultiplier, exploding);
            }

            else
            {
                if (playerVars.unitAmount + unitValue <= constants.maxUnitAmount)
                {
                    GameObject unit = PhotonNetwork.Instantiate(unitToSpawn, spawnLocation.position, spawnLocation.rotation, 0);
                    unit.GetComponent<PhotonView>().RPC("SyncValues", PhotonTargets.All, damageMultiplier, healthMultiplier, exploding);
                    playerVars.changeUnitAmount((int)unitValue);
                }
            }
        }
	}
}

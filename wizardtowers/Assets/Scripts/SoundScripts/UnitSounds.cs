﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitSounds : MonoBehaviour {

	[FMODUnity.EventRef]
	public string attack;

	[FMODUnity.EventRef]
	public string hit;

	[FMODUnity.EventRef]
	public string death;

    [FMODUnity.EventRef]
    public string sporeExplosion;

	[FMODUnity.EventRef]
	public string emmettSacrifice = "event:/units/sacrifice_emmett";

	[FMODUnity.EventRef]
	public string otherSacrifice = "event:/units/sacrifice_small";

    //The sound of a swing
    public void AttackSound()
    {
		if (FmodEventChecker.EventExists (attack)) {
			FMOD.Studio.EventInstance atsound = FMODUnity.RuntimeManager.CreateInstance (attack);
			FMODUnity.RuntimeManager.AttachInstanceToGameObject (atsound, GetComponent<Transform> (), GetComponent<Rigidbody> ());
			atsound.start ();
			atsound.release ();
		}
	}

	public void DeathSound()
    {
		if (FmodEventChecker.EventExists (death)) {
			FMOD.Studio.EventInstance dsound = FMODUnity.RuntimeManager.CreateInstance (death);
			FMODUnity.RuntimeManager.AttachInstanceToGameObject (dsound, GetComponent<Transform> (), GetComponent<Rigidbody> ());
			dsound.start ();
			dsound.release ();
		}
	}

    public void SporeExplosion()
    {
        if (FmodEventChecker.EventExists(sporeExplosion))
        {
            FMOD.Studio.EventInstance sporesound = FMODUnity.RuntimeManager.CreateInstance(sporeExplosion);
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(sporesound, GetComponent<Transform>(), GetComponent<Rigidbody>());
            sporesound.start();
            sporesound.release();
        }
    }

	public void SacrificeSound() {
		if (gameObject.GetComponent<UnitAI> ().unitType == UnitAI.unitTypes.Emmett) {
			if (FmodEventChecker.EventExists (emmettSacrifice)) {
				FMODUnity.RuntimeManager.PlayOneShot (emmettSacrifice, gameObject.transform.position);
			}
		} else {
			if (FmodEventChecker.EventExists (otherSacrifice)) {
				FMODUnity.RuntimeManager.PlayOneShot (otherSacrifice, gameObject.transform.position);
			}
		}
	}

    //The sound of a hit connecting
    //Since we can't set parameter values for one-shots, we have to do it in a roundabout way
    public void HitSound(GameObject target) {
		if(FmodEventChecker.EventExists(death)) {
			FMOD.Studio.EventInstance hitsound = FMODUnity.RuntimeManager.CreateInstance(hit);
			if (target != null) {
				FMODUnity.RuntimeManager.AttachInstanceToGameObject (hitsound, target.GetComponent<Transform> (), target.GetComponent<Rigidbody> ());
			}
			hitsound.start();
			hitsound.setParameterValue ("impact_type", HitType (target));
			hitsound.release();  	//Let it go, let it go~
		}
	}


	//Returns the parameter value for hitting different kinds of targets
	public float HitType( GameObject target )
    {
		if (target != null && target.GetComponent<UnitAI>() != null)
        {
			UnitAI.unitTypes targetType = target.GetComponent<UnitAI> ().unitType;
			switch (targetType) {
			case UnitAI.unitTypes.Emmett:
				return 1.0f;

			case UnitAI.unitTypes.Lamiel:
				return 2.0f;

			case UnitAI.unitTypes.Sarko:
				return 3.0f;

			default:
				return 0.0f;
			}
		}
        else
        {
            return 0f;
        }			
	}
}

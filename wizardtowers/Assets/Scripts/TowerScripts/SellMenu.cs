﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellMenu : MonoBehaviour {

	public TowerPieceUI piece;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonUp (0)) {
			Destroy (this.gameObject);
		}
	}

	public void sell(){
		piece.emptyRoom ();
	}
}

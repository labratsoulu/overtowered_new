﻿using UnityEngine;
using System.Collections;

public class UnitSpeech : MonoBehaviour {

	private string golemSound = "event:/Units/Golem/Voice lines";
	private string cultistSound = "event:/Units/Apprentice/Voice lines";
	private string skeletonSound = "event:/Units/Skeleton/Voice lines";

	// Use this for initialization
	void Start () {
	
	}

	public void playGolem(){
		FMODUnity.RuntimeManager.PlayOneShot (golemSound, transform.position);
	}

	public void playCultist(){
		FMODUnity.RuntimeManager.PlayOneShot (cultistSound, transform.position);
	}

	public void playSkeleton(){
		FMODUnity.RuntimeManager.PlayOneShot (skeletonSound, transform.position);
	}

}

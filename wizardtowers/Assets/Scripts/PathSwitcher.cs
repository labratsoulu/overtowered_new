﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathSwitcher : MonoBehaviour {

    public int selectedLane;

    void Awake()
    {
        selectedLane = 2;
    }
	
    public void SwitchLane(int laneIndex)
    {
        selectedLane = laneIndex;
    }
}

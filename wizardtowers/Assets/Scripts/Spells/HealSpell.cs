﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class HealSpell : MonoBehaviour {

	protected RaycastHit hit;

	public TowerPieceUI piece;

	[FMODUnity.EventRef]
	public string sfxEvent ="event:/spells/heal";

    private Camera cam;
    private GameObject playerCamera;
    private Constants constants;
	private LayerMask unitLayer;
    private LayerMask deadLayer;
	private PlayerVars playerVars;
    private PhotonView animView;
	private string spellsound = "event:/Spells/Healing";

	void Awake ()
	{
		if (PhotonNetwork.player.ID == 1)
		{
			//blue player
			unitLayer = LayerMask.NameToLayer("Player1");
            deadLayer = LayerMask.NameToLayer("Player1Dead");
            playerVars = GameObject.FindWithTag("Player1").GetComponent<PlayerVars>();
            playerCamera = GameObject.FindWithTag("Player1Camera");
        }
		else // playerID == 2
		{
			unitLayer = LayerMask.NameToLayer("Player2");
            deadLayer = LayerMask.NameToLayer("Player2Dead");
            playerVars = GameObject.FindWithTag("Player2").GetComponent<PlayerVars>();
            playerCamera = GameObject.FindWithTag("Player2Camera");
        }

        cam = playerCamera.GetComponent<Camera>();
        animView = playerCamera.GetComponent<PhotonView>();
        constants = GameObject.FindGameObjectWithTag("Constants").GetComponent<Constants>();
    }

	// Update is called once per frame
	void Update()
    {
		Ray ray = cam.ScreenPointToRay(Input.mousePosition);

		if (Physics.Raycast(ray, out hit))
		{
			transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
		}

		EndDrag();
	}

	protected void EndDrag()
	{
		if (Input.GetMouseButtonDown (0) && !EventSystem.current.IsPointerOverGameObject()) {
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().casting ();
			playerVars.calculateMagic(Mathf.Round(-constants.healSpellCost * piece.spellCostDown));
			piece.resetTimer ();
			if (hit.transform != null)
            {
				PhotonNetwork.Instantiate("HealSpellEffect", transform.position + new Vector3(0, 3f, 0), Quaternion.Euler(90, 0, 0), 0);
                animView.RPC("SetAnimatorTrigger", PhotonTargets.All, "HealCast");

				FMOD.Studio.EventInstance sfxInstance = null;
				if (FmodEventChecker.EventExists (sfxEvent)) {
					sfxInstance = FMODUnity.RuntimeManager.CreateInstance (sfxEvent);
				}
				if(sfxInstance != null) {
					sfxInstance.set3DAttributes (FMODUnity.RuntimeUtils.To3DAttributes (hit.transform));
					sfxInstance.start ();
					sfxInstance.release ();
				}

				Collider[] hitColliders = Physics.OverlapSphere (transform.position, constants.spellRadius);
				foreach (Collider unit in hitColliders)
                {
					if (unit.gameObject.layer == unitLayer || unit.gameObject.layer == deadLayer)
                    {
						unit.gameObject.GetComponent<PhotonView>().RPC("HealPercentage", PhotonTargets.All, constants.healSpellHealing * piece.spellEffectUp);
                        unit.gameObject.GetComponent<PhotonView>().RPC("ActivateEffect", PhotonTargets.All, 1);
                    }
                }
			}

			Destroy(gameObject);
		}

        else if (Input.GetMouseButtonDown(1))
        {
			Destroy(gameObject); 
		}
	}
}

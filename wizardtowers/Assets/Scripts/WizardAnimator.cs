﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardAnimator : MonoBehaviour
{
    private PhotonView thisView;
    private GameObject selectedWizard;
    private Animator animator;

    public GameObject[] wizards;

    //Toggled components
    public MonoBehaviour[] scriptsToEnable;
    public Camera cam;
    public GUILayer guiLayer;
    public FlareLayer flareLayer;

    //public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    if (stream.isWriting)
    //    {
    //        stream.SendNext(animator.sett);
    //        stream.SendNext(healCast);
    //        stream.SendNext(boostCast);
    //    }

    //    else // stream.isReading
    //    {
    //        damageCast = (bool)stream.ReceiveNext();
    //        healCast = (bool)stream.ReceiveNext();
    //        boostCast = (bool)stream.ReceiveNext();
    //    }
    //}

    void Awake()
    {
        thisView = GetComponent<PhotonView>();
        selectedWizard = wizards[(int)PhotonPlayer.Find(thisView.ownerId).CustomProperties["selectedWizard"]];
        selectedWizard.SetActive(true);
        animator = selectedWizard.GetComponent<Animator>();

        if (thisView.isMine)
        {
            cam.enabled = true;
            guiLayer.enabled = true;
            flareLayer.enabled = true;

            foreach (MonoBehaviour script in scriptsToEnable)
            {
                script.enabled = true;
            }

            if (PhotonNetwork.player.ID == 1)
            {
                gameObject.tag = "Player1Camera";
                cam.cullingMask = ~(1 << 13);

                foreach (Transform trans in selectedWizard.GetComponentsInChildren<Transform>())
                {
                    trans.gameObject.layer = 13;
                }
            }

            else // ID == 2 
            {
                gameObject.tag = "Player2Camera";
                cam.cullingMask = ~(1 << 14);

                foreach (Transform trans in selectedWizard.GetComponentsInChildren<Transform>())
                {
                    trans.gameObject.layer = 14;
                }
            }
        }     
    }

    [PunRPC]
    public void SetAnimatorTrigger(string triggerName)
    {
        animator.SetTrigger(triggerName);
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

	/*
	 * Here we have the methods and hooks for the various sounds used in the menu system.
	 * At the time of writing (5.4.) this script resides in the SoundManager object in the lobby scene
	 * and is called from the UI elements
	 */

public class UISounds : MonoBehaviour {


	[FMODUnity.EventRef]
	public string lightClick;

	public void LightClick() {
		CheckAndPlayEvent (lightClick);
	}

	[FMODUnity.EventRef]
	public string heavyClick;

	public void HeavyClick() {
		CheckAndPlayEvent (heavyClick);
	}

	[FMODUnity.EventRef]
	public string hoverEnter;

	public void HoverEnter() {
		CheckAndPlayEvent (hoverEnter);
	}

	[FMODUnity.EventRef]
	public string menuForward;

	public void MenuForward() {
		CheckAndPlayEvent (menuForward);
	}

	[FMODUnity.EventRef]
	public string menuBack;

	public void MenuBack() {
		CheckAndPlayEvent (menuBack);
	}

	[FMODUnity.EventRef]
	public string gameStart;

	public void GameStart() {
		CheckAndPlayEvent (gameStart);
	}

	[FMODUnity.EventRef]
	public string selectionChange;

	public void SelectionChange() {
		CheckAndPlayEvent (selectionChange);
	}

	[FMODUnity.EventRef]
	public string textboxEdited;

	public void TextboxEdited() {
		CheckAndPlayEvent (textboxEdited);
	}

	[FMODUnity.EventRef]
	public string radialMenuOpen;

	public void RadialMenuOpen() {
		CheckAndPlayEvent (radialMenuOpen);
	}

	[FMODUnity.EventRef]
	public string gameMenuOpen;

	public void GameMenuOpen() {
		CheckAndPlayEvent (gameMenuOpen);
	}

	[FMODUnity.EventRef]
	public string gameMenuClose;

	public void GameMenuClose() {
		CheckAndPlayEvent (gameMenuClose);
	}

	[FMODUnity.EventRef]
	public string exitMatch;

	public void ExitMatch() {
		CheckAndPlayEvent (exitMatch);
	}

	[FMODUnity.EventRef]
	public string radialMenuClick;

	public void RadialMenuClick() {
		CheckAndPlayEvent (radialMenuClick);
	}

	[FMODUnity.EventRef]
	public string productionPause;

	public void ProductionPause() {
		CheckAndPlayEvent (productionPause);
	}

	[FMODUnity.EventRef]
	public string productionResume;

	public void ProductionResume() {
		CheckAndPlayEvent (productionResume);
	}

	[FMODUnity.EventRef]
	public string testCoin;

	public void TestCoin() {
		CheckAndPlayEvent (testCoin);
	}

	void CheckAndPlayEvent(string eventPath) {
		FMOD.Studio.EventInstance eInst = null;
		if (FmodEventChecker.EventExists (eventPath)) {
			eInst = FMODUnity.RuntimeManager.CreateInstance (eventPath);
		}
		if (eInst != null) {
			eInst.start ();
			eInst.release ();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHealthBar : MonoBehaviour {

	//toggle using TAB
	//have it update to correct size
	//take into account max health

	private UnitAI unitAI;
	private float maxHealth;
	private SpriteRenderer sprite;

	void Awake () {
		sprite = GetComponent<SpriteRenderer> ();
		unitAI = transform.parent.GetComponent<UnitAI> ();
		maxHealth = unitAI.health;
	}

	void Update(){
		if (GameManager.healthBarOn) {
			if (!sprite.enabled)
				sprite.enabled = true;
			Vector3 scale = transform.localScale;
			scale.x = (unitAI.health / maxHealth) / 2;
			transform.localScale = scale;
		} else if(sprite.enabled) {
			sprite.enabled = false;
		}
	}

	void LateUpdate () {
		transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward,
			Camera.main.transform.rotation * Vector3.up);
	}
}

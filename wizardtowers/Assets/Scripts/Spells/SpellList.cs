﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpellList : MonoBehaviour {

    public PlayerVars player1Vars;
    public PlayerVars player2Vars;
    private PlayerVars playerVars;

    public GameObject ArcaneBomb;
	public GameObject BoostSpell;
	public GameObject DamageSpell;
	public GameObject HealSpell;

	public GameObject noMagicText;

	public Constants constants;

//	private List<TowerPieceUI> boostSpellPieces;
//	private List<TowerPieceUI> damageSpellPieces;
//	private List<TowerPieceUI> healSpellPieces;

	void Awake(){
		if (PhotonNetwork.player.ID == 1)
		{
			playerVars = player1Vars;
		}

		else // ID == 2
		{
			playerVars = player2Vars;
		}

//		boostSpellPieces = new List<TowerPieceUI> ();
//		damageSpellPieces = new List<TowerPieceUI> ();
//		healSpellPieces = new List<TowerPieceUI> ();
	}

	public void arcaneBomb(){
        if (playerVars.arcaneBombs != 0)
        {
            Instantiate(ArcaneBomb);
        }	
	}

	public void boostSpell(TowerPieceUI piece){
		if (playerVars.magic >= (constants.boostSpellCost * piece.spellCostDown)) {
			if (GameObject.FindGameObjectWithTag ("BoostSpell") == null) {
				GameObject temp = Instantiate (BoostSpell);
				temp.GetComponent<BoostSpell> ().piece = piece;
			}
		} else{
			//not enough magic
			noMagic();
		}
	}

	public void damageSpell(TowerPieceUI piece){
		if (playerVars.magic >= (constants.damageSpellCost * piece.spellCostDown)) {
			if (GameObject.FindGameObjectWithTag ("DamageSpell") == null) {
				GameObject temp = Instantiate (DamageSpell);
				temp.GetComponent<DamageSpell> ().piece = piece;
			}
		} else{
			//not enough magic
			noMagic();
		}
	}

	public void healSpell(TowerPieceUI piece){
		if (playerVars.magic >= (constants.healSpellCost * piece.spellCostDown)) {
			if (GameObject.FindGameObjectWithTag ("HealSpell") == null) {
				GameObject temp = Instantiate (HealSpell);
				temp.GetComponent<HealSpell> ().piece = piece;
			}
		} else{
			//not enough magic
			noMagic();
		}
	}


	public void noMagic(){
		noMagicText.SetActive (true);
		noMagicText.GetComponent<NoMagicText> ().showText ();
		GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().noMagic ();
	}
//
//	public void addBoostSpellPiece(TowerPieceUI piece){
//		boostSpellPieces.Add (piece);
//	}
//
//	public void addDamageSpellPiece(TowerPieceUI piece){
//		boostSpellPieces.Add (piece);
//	}
//
//	public void addHealSpellPiece(TowerPieceUI piece){
//		boostSpellPieces.Add (piece);
//	}
}

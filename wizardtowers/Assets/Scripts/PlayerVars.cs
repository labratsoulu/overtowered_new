﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerVars : MonoBehaviour {

    public float health;
    public float magic;
    public float arcaneBombs;
    public float maxMagic;
	public float magicRechargeMultiplier = 1;
    public float unitAmount;
    public bool recharging = false;
    public GameObject playerBase;
    public PhotonView networkPhotonView;
    public Constants constants;
    public Text unitAmountText;

    private PhotonView thisView;
    private NetworkInGame manager;
    private TowerShaker towerShaker;
	private TowerSounds towerSounds;

    void Awake()
    {
        thisView = GetComponent<PhotonView>();
        health = constants.towerStartHealth;
        magic = constants.magicStartAmount;
        arcaneBombs = constants.arcaneBombAmount;
        maxMagic = constants.maxMagic;
        unitAmount = 0;
        unitAmountText.text = "Units: " + unitAmount + " / " + constants.maxUnitAmount;
        networkPhotonView = GameObject.FindWithTag("NetworkManager").GetComponent<PhotonView>();
        manager = GameObject.FindWithTag("NetworkManager").GetComponent<NetworkInGame>();
        towerShaker = GetComponent<TowerShaker>();
		towerSounds = GetComponent<TowerSounds>();
    }

    void Start()
    {
        // Initial start of the recharge timer, if magic is not already at max.
        if (magic != maxMagic)
        {
            StartCoroutine("StartRechargeTimer");
        }
    }

    private IEnumerator StartRechargeTimer()
    {
        yield return new WaitForSeconds(constants.rechargeCooldown);
        StartCoroutine("RechargeMagic");
    }

    private IEnumerator RechargeMagic()
    {
        while (true)
        {
            // Stop recharging if at max.
            if (magic == maxMagic)
            {
                yield break;
            }

            yield return new WaitForSeconds(constants.magicRechargeStep);
			calculateMagic(Mathf.Round(constants.magicRechargeRate * magicRechargeMultiplier));            
        }     
    }

    // Handles player tower taking damage
    [PunRPC]
    public void TakeDamage(float amount)
    {
        if (health > 0f)
        {
            health -= amount;
            health = Mathf.Clamp(health, 0f, constants.towerStartHealth);

            if (health > 0f)
            {
                towerShaker.Shake(amount);
            }

            if (constants.offlineMode)
            {
                if (thisView.owner != null)
                {
                    networkPhotonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, 1, health);
                }

                else
                {
                    networkPhotonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, 2, health);
                }

                if (health == 0)
                {
					towerSounds.Destruction ();
                    towerShaker.StartCoroutine("ExplodeBase");
                    //playerBase.SetActive(false);
                    networkPhotonView.RPC("EndGame", PhotonTargets.All, playerBase.transform.GetChild(0).GetComponent<TowerBaseModels>().ownerID);
                }
            }

            else
            {
                networkPhotonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, thisView.owner.ID, health);

				if (health == 0)
                {
					towerSounds.Destruction();
                    towerShaker.StartCoroutine("ExplodeBase");
                    //playerBase.SetActive (false);
                    if (thisView.isMine)
                    {
						networkPhotonView.RPC ("EndGame", PhotonTargets.All, thisView.owner.ID);
					}
				}
            }
        }      
    }

    // Handles increasing/decreasing magic 
    [PunRPC]
    public void calculateMagic(float amount)
    {
        // Change magic value and make sure it stays within defined bounds.
        magic += amount;
        magic = Mathf.Clamp(magic, 0f, maxMagic);

        // Stop recharging for this player only if magic decreases.
        if (thisView.isMine && amount < 0)
        {
            StopAllCoroutines();
            StartCoroutine("StartRechargeTimer");
        }

		manager.UpdateMagicValue(magic, maxMagic);
    }

    public void changeUnitAmount(int amount)
    {
        unitAmount += amount;
        unitAmountText.text = "Units: " + unitAmount + " / " + constants.maxUnitAmount;
    }
}
